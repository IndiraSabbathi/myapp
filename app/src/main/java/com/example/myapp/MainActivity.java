package com.example.myapp;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class MainActivity extends AppCompatActivity {

    Button buttonToNavigate;
    TextView txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonToNavigate = (Button) findViewById(R.id.buttonToNavigate);


        buttonToNavigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("mcid://MCID/verify/mcid=12345&&mcApp=MyApp")));
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("mcid://MCID/verifym?mcid=1234&tpname=amazon")));
            }
        });

    }

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 1) {
            String age = data.getStringExtra("Age");
            //String mMessage = mBundle.getString("Age");
            System.out.println("======================" +age);
        }
    }

    public void openBrowser(View view){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://192.168.0.4:8080/mastercard/op/oauth?isAge>25&target_url=app://amazon.com/checkout?checkout=12"));
        startActivity(browserIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("is this method calling");
        Intent intent = getIntent();
        System.out.println("Intent data here "+intent);
        if (intent != null && intent.getAction() != null && intent.getAction().equals("com.mastercard.id.age")) {

            // String action = i.getAction();
            Bundle bundle = intent.getBundleExtra("data");
            String age = null;
            if(bundle != null) {
                age = bundle.getString("age");
            }
            // String type = i.getType();
            txt = (TextView) findViewById(R.id.age);
            txt.setText(age);
            System.out.println("age back  :" + age);

            //retrive age details from SQLLIte database

            String URL = "content://com.example.myapp.DigitalIDAgeValidationProvider";
            Uri data = Uri.parse(URL);
            Cursor c = getContentResolver().query(data, null, null, null, null);
            String myData = c.getString(c.getColumnIndex(DigitalIDAgeValidationProvider.AGE));
            Toast.makeText(this, myData, Toast.LENGTH_SHORT).show();




        }
       /* if (Intent.ACTION_VIEW.equals(action)&& type != null){
            if("text/palin".equals(type)){
                String age = i.getStringExtra(Intent.EXTRA_TEXT);
                txt.setText("Age "+age);
            }
        }*/
    }
}